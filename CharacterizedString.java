import java.util.Scanner;

public class CharacterizedString
{
    public static void main (String[] args)
    {
        Scanner scanName = new Scanner(System.in);
        System.out.println("Silahkan ketik nama, lalu tekan enter:");
        String insertName = scanName.nextLine();
        System.out.println("Sedang memproses pemecahan nama menjadi karakter...");
        System.out.println("Done!");
        for (int i=0; i < insertName.length(); i++)
        {
            System.out.println("Karakter" + "[" + (i+1) + "]" + ": " + insertName.charAt(i));
        }
    }
}